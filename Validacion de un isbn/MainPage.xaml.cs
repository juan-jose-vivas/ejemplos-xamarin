using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace xamarin_isbn
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.btn_validar.Clicked += btn_validar_Clicked;
        }


        private void btn_validar_Clicked(object sender, EventArgs e)
        {




            int suma = 0;
            for (int i = 0; i < txt_isbn.Text.Length; i++)
            {
                int num = int.Parse(txt_isbn.Text.Substring(i, 1));



                int multiplicacion = num * (i + 1);
                suma += multiplicacion;
                //Abreviatura de suma=suma+multiplicacion; 
            }


            if (suma % 11 == 0)
            {
                lbl_resultado.Text = "ISBN CORRECTO";
            }
            else
            {
                lbl_resultado.Text = "ISBN INCORRECTO";


            }
        }
    }
}
