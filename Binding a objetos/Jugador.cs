using System;
namespace xamarin_bindings3.Models
{
    public class Jugador
    {
        public Jugador()
        {
            this.Nombre = "Guti";
            this.Equipo = "Real Madrid";
            this.Imagen = "Mi imagen";
            this.Descripcion = "Genio del pase y visión de juego.  El jugador con más calidad de toda la plantilla (Zidane, Ronaldo Nazario dixit...)";
        }
        public String Nombre { get; set; }
        public String Equipo { get; set; }
        public String Imagen { get; set; }
        public String Descripcion { get; set; }
    }
}
