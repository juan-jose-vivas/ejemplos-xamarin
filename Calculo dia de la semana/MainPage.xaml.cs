using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;




namespace xamarin_dia_semana

{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage
        : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            for (int i = 1850; i <= DateTime.Now.Year; i++)
            {
                picYear.Items.Add(i.ToString());
            }

            List<String> mes30 = new List<string> { "4", "6", "9", "11" };

            this.picYear.SelectedIndexChanged += (sender, arg) =>
            {

                picMonth.Items.Clear();

                int t = picYear.SelectedIndex;
                if (t != -1)
                {
                    if (picYear.Items[t] == DateTime.Now.Year.ToString())
                    {
                        for (int i = 1; i <= DateTime.Now.Month; i++)
                        {
                            picMonth.Items.Add(i.ToString());
                            picMonth.IsEnabled = true;
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= 12; i++)
                        {
                            picMonth.Items.Add(i.ToString());
                            picMonth.IsEnabled = true;

                        }
                    }

                }

            };
            this.picDay.SelectedIndexChanged += PicDay_SelectedIndexChanged;
            this.picMonth.SelectedIndexChanged += (sender, args) => {
                if (picMonth.SelectedIndex != -1)
                {
                    picDay.Items.Clear();
                    int a = int.Parse(picYear.Items[picYear.SelectedIndex]);
                    if (a % 4 == 0 && (a % 100 != 0 || a % 400 == 0))
                    {
                        if (a == DateTime.Now.Year && (int.Parse(picMonth.Items[picMonth.SelectedIndex]) == DateTime.Now.Month))
                        {
                            for (int i = 1; i <= DateTime.Now.Day; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                        else if (picMonth.Items[picMonth.SelectedIndex] == 2.ToString())
                        {
                            for (int i = 1; i <= 29; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                        else if (mes30.Contains(picMonth.Items[picMonth.SelectedIndex]))
                        {
                            for (int i = 1; i <= 30; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                    }
                    else
                    {
                        if (a == DateTime.Now.Year && (int.Parse(picMonth.Items[picMonth.SelectedIndex]) == DateTime.Now.Month))
                        {
                            for (int i = 1; i <= DateTime.Now.Day; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                        else if (picMonth.Items[picMonth.SelectedIndex] == "2")
                        {
                            for (int i = 1; i <= 28; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                        else if (mes30.Contains(picMonth.Items[picMonth.SelectedIndex]))
                        {
                            for (int i = 1; i <= 30; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                picDay.Items.Add(i.ToString());
                            }
                        }
                    }
                }
                picDay.IsEnabled = true;
            };




            this.btncalc.Clicked += Btncalc_Clicked;


        }

        private void Btncalc_Clicked(object sender, EventArgs e)
        {
            DateTime d = new DateTime(int.Parse(picYear.Items[picYear.SelectedIndex]), int.Parse(picMonth.Items[picMonth.SelectedIndex]), int.Parse(picDay.Items[picDay.SelectedIndex]));
            diafin.Text = d.DayOfWeek.ToString();
        }

        private void PicDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btncalc.IsEnabled = true;
        }
    }
}
